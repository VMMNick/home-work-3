//Task 1
let userNumber = parseInt(prompt("Будь ласка, введіть число:"));
if (!isNaN(userNumber)) {
    console.log(`Числа, кратні 5, у діапазоні від 0 до ${userNumber}:`);
    
    var numbers = [];
    for (var i = 0; i <= userNumber; i++) {
        if (i % 5 === 0) {
            numbers.push(i);
        }
    }
    if (numbers.length > 0) {
        console.log(numbers.join(', '));
    } else {
        console.log("Sorry, no numbers");
    }
} else {
    console.log("Ви ввели некоректне число.");
}
